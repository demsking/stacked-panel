/**
 * HPX Stacked Panel - A fully stylized StackedWidget implementation
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HPXSTACKEDPANEL_COMPONENTS_STACKEDWIDGET_H_
#define HPXSTACKEDPANEL_COMPONENTS_STACKEDWIDGET_H_

#include <QWidget>

#include "global.h"  // NOLINT(build/include)

namespace HPX {
  class StackedPanelToolbox;

  class LIBHPXSTACKEDPANEL_EXPORT StackedPanelWidget: public QWidget {
    Q_OBJECT

    StackedPanelToolbox* _toolbox = nullptr;
    bool _active = false;

    protected:
      explicit StackedPanelWidget(bool disableProperty, QWidget* parent = nullptr);

    public:
      explicit StackedPanelWidget(QWidget* parent = nullptr);
      virtual ~StackedPanelWidget();

      bool isActive() const;
      StackedPanelToolbox* toolbox() const;

      virtual void setFocus();

    signals:
      void focusIn();
      void focusOut();
  };
} // namespace HPX

#endif // HPXSTACKEDPANEL_COMPONENTS_STACKEDWIDGET_H_
